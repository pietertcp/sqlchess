SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MovePice]
	@FromSqr NVARCHAR(2),
	@ToSqr NVARCHAR(2),
	@PlayerColor NVARCHAR(1)
AS
BEGIN

	DECLARE @Frclm NVARCHAR(1),
	        @Frrw NVARCHAR(1),
	        @FbitClm NVARCHAR(4),
			--To
			@Toclm NVARCHAR(1),
	        @Torw NVARCHAR(1),
	        @TbitClm NVARCHAR(4),
			
			@colorValue NVARCHAR(1);
	
	IF(@PlayerColor = 'B')
	BEGIN
		SET @colorValue = '1';
	END;

	ELSE IF(@PlayerColor = 'W')
	BEGIN
		SET @colorValue = '2';
	END;

	SET @Frclm = SUBSTRING(@FromSqr,1,1);
	SET @Frrw = SUBSTRING(@FromSqr,2,1);
	
	SET @Toclm = SUBSTRING(@FromSqr,1,1);
	SET @Torw = SUBSTRING(@FromSqr,2,1);
	
	SELECT @Frclm, @FbitClm, @Frrw, @Toclm, @Torw, @TbitClm
	
	DECLARE @frmsql NVARCHAR(150);
	DECLARE @tosql NVARCHAR(150);
	
	SET @frmsql = 'UPDATE [dbo].[ChessBoard] SET ' + @FbitClm + ' = ''0'' WHERE [RowID] = ' + @Frrw + '';
	SET @tosql = 'UPDATE [dbo].[ChessBoard] SET ' + @TbitClm + ' = '''+ @colorValue +''' WHERE [RowID] = ' + @Torw + '';
	
	EXECUTE sp_executesql @frmsql;
	EXECUTE sp_executesql @tosql;

END