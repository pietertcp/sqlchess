/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [RowID]
      ,CASE
			WHEN (
					SELECT [Icon] 
					FROM [dbo].[ChessPiecesList] 
					WHERE [ID] = [A]
				 ) IS NULL THEN N''
			ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [A])
	   END  AS [A]
      ,CASE
	        WHEN (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [B]) IS NULL THEN N''
	        ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [B])
	   END  AS [B]
      ,CASE
	        WHEN (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [C]) IS NULL THEN N''
	        ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [C])
	   END  AS [C]
      ,CASE
	        WHEN (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [D]) IS NULL THEN N''
	        ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [D])
	   END  AS [D]
      ,CASE
	        WHEN (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [E]) IS NULL THEN N''
	        ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [E])
	   END  AS [E]
      ,CASE
	        WHEN (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [F]) IS NULL THEN N''
	        ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [F])
	   END  AS [F]
      ,CASE
	        WHEN (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [G]) IS NULL THEN N''
	        ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [G])
	   END  AS [G]
      ,CASE
	        WHEN (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [H]) IS NULL THEN N''
	        ELSE (SELECT [Icon] FROM [dbo].[ChessPiecesList] WHERE [ID] = [H])
	   END  AS [H]
  FROM [InfoFleetSQL].[dbo].[ChessBoard] AS B